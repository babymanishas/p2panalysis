const {createUser} = require("nkeys.js");
const fs = require('fs');

const pubArray = [];

for(let i=1; i<=100; i++){
    let user = createUser();
    let pubKey = user.getPublicKey();
    let priKey = user.getPrivateKey();
    let seed = user.getSeed();

    let dir = `keys/${i}`;

    if (!fs.existsSync(dir)){
        fs.mkdirSync(dir);
    }

    fs.writeFile(`keys/${i}/pubKey.pem`, pubKey, (err) => {
        if (err) 
            return console.log(err);
    });

    fs.writeFile(`keys/${i}/priKey.pem`, priKey, (err) => {
        if (err) 
            return console.log(err);
    });

    fs.writeFile(`keys/${i}/seed.pem`, seed, (err) => {
        if (err) 
            return console.log(err);
    });

    pubArray.push(pubKey);
}

fs.writeFile(`knownIdentities.json`, JSON.stringify(pubArray), (err) => {
    if (err) 
        return console.log(err);
});