const { connect, StringCodec } = require("nats");
const { fromPublic, fromSeed } = require("nkeys.js");
const fs = require('fs');

const userDir = process.argv[2];

const pubKey = fs.readFileSync(`keys/${userDir}/pubKey.pem`).toString();
const seed = fs.readFileSync(`keys/${userDir}/seed.pem`);
const knownIdentities = JSON.parse(fs.readFileSync(`knownIdentities.json`));


const waitFor = delay => new Promise(resolve => setTimeout(resolve, delay));

async function main(){
    // to create a connection to a nats-server:
    const nc = await connect({ servers: "localhost:4222"});

    // create a codec
    const sc = StringCodec();
    // create a simple subscriber and iterate over messages
    // matching the subscription
    const sub = nc.subscribe("hello.*");
    (async () => {
        for await (const m of sub) {
            let revdPubKey = m.subject.split(".")[1];
            // console.log(`${m.subject} => [${sub.getProcessed()}]: ${sc.decode(m.data)}`);

            if(knownIdentities.indexOf(revdPubKey) == -1) {
                console.log("💀");
                continue;
            }

            if(revdPubKey == pubKey) continue;

            let rData = JSON.parse(sc.decode(m.data));
            let pk = fromPublic(revdPubKey)
            let verification = pk.verify(Buffer.from(rData['msg']), Buffer(rData['sign'], 'base64'))
            if(verification){
                console.log(`${new Date().getTime()-rData['inTime']},${rData['pid']},✅`);
            }else{
                console.log(`${new Date().getTime()-rData['inTime']},${rData['pid']},❌`);
            }
        }
        console.log("subscription closed");
    })();


    await waitFor(knownIdentities.length);

    let sk = fromSeed(Buffer.from(seed));

    for (let index = 1; index <= 10; index++) {
        let data = `my message is-${pubKey}-${index}`;

        let smg = sk.sign(Buffer.from(data));
    
        let msgData = {
            'msg': data,
            'sign': new Buffer(smg).toString('base64'),
            'inTime': new Date().getTime(),
            'count': index,
            'pid': userDir
        }
        nc.publish(`hello.${pubKey}`, sc.encode(JSON.stringify(msgData)));
        await waitFor(10);
    }

    setTimeout(async() => {
        await nc.drain();
    }, 50000)

    // we want to insure that messages that are in flight
    // get processed, so we are going to drain the
    // connection. Drain is the same as close, but makes
    // sure that all messages in flight get seen
    // by the iterator. After calling drain on the connection
    // the connection closes.
    
}
main();